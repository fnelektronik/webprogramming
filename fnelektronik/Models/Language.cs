//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace fnelektronik.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Language
    {
        public int LanguageId { get; set; }
        public Nullable<int> Turkish1 { get; set; }
        public Nullable<int> English2 { get; set; }
    }
}
