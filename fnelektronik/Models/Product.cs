//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace fnelektronik.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Product
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Product()
        {
            this.Order_Detail = new HashSet<Order_Detail>();
        }
    
        public int ProductId { get; set; }
        public string ProductNameTR { get; set; }
        public string ProductNameEN { get; set; }
        public byte[] ProductImage1 { get; set; }
        public byte[] ProductImage2 { get; set; }
        public string ProductDetailsTR { get; set; }
        public string ProductDetailsEN { get; set; }
        public Nullable<int> ProductPrice { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Order_Detail> Order_Detail { get; set; }
    }
}
